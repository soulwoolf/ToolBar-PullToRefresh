package net.soulwolf.toolbar.pulltorefresh.sdk;

import android.view.View;

class CompatBase {

    static void setAlpha(View view, float alpha) {
        // NO-OP
    }

    static void postOnAnimation(View view, Runnable runnable) {
        view.postDelayed(runnable, 10l);
    }

}
